package kraig.data;

import kraig.data.AverageRating;

import java.util.ArrayList;

public class TopUSort {
  public AverageRating rating;

  public TopUSort(AverageRating rating) {
    this.rating = rating;
  }

  public String output() {
      //MapReduce compares lexographically so it also compares same-width numbers in strings just fine.
      // Key looks like 3.4 0001244 which will sort by lowest reviews first then by lowest user ids
      return String.valueOf(rating.averageRating) + rating.id;
  }
}
