package kraig.data;

import kraig.MovieMetaDataService;
import kraig.data.AverageRating;

import java.util.ArrayList;

public class TopMSort {
  public static MovieMetaDataService metaService = new MovieMetaDataService();

  public AverageRating rating;

  public TopMSort(AverageRating rating) {
    this.rating = rating;
  }

  public String output() {
      MovieMetaData meta = TopMSort.metaService.getMovieMetaData(rating.id);
      String tieSortString = (9999 - meta.year) + meta.title;
      // Sort string looks like 1.6(5-rating) 7983  Movie; which sorts higher ratings first, then newest movies first, then alphabetically
      return String.valueOf(5-rating.averageRating) + tieSortString;
  }
}
