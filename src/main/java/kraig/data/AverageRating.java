package kraig.data;

import kraig.data.Rating;

import java.util.ArrayList;

public class AverageRating {
  public String id;
  public float averageRating;
  public int count;

  public AverageRating(String csv) {
    String[] values = csv.split(",");
    if (values.length != 3)
      throw new RuntimeException("input format invalid!");
    this.id = values[0];
    this.averageRating = Float.parseFloat(values[1]);
    this.count = Integer.parseInt(values[2]);
  }

  public AverageRating(String id, ArrayList<Rating> ratings) {
    this.id = id;
    this.count = ratings.size();

    int sum = 0;
    for (Rating rating : ratings) {
        sum += rating.rating;
    }

    this.averageRating = (float)sum / this.count;
  }

  public String output() {
      return id + "," + averageRating + "," + count;
  }
}
