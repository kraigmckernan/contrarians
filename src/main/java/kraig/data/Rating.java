package kraig.data;

public class Rating {
  public String userID;
  public int rating;
  public String date;
  public String movieID;

  public Rating(int rating) {
    this.rating = rating;
    this.userID = "-1";
    this.date = "-1";
    this.movieID = "-1";
  }

  public Rating(String csv) {
    String[] values = csv.split(",");
    if (values.length != 4)
      throw new RuntimeException("input format invalid!");
    this.userID = values[0];
    this.rating = Integer.parseInt(values[1]);
    this.date = values[2];
    this.movieID = values[3];
  }

  public String output() {
      return this.userID + "," + this.rating + "," + this.date + "," + this.movieID;
  }
}
