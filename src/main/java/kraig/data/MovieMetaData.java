package kraig.data;

public class MovieMetaData {
  public String movieID;
  public int year;
  public String title;

  public MovieMetaData(String csv) {
    String[] values = csv.split(",", 3);
    if (values.length != 3)
      throw new RuntimeException("input format invalid!");
    this.movieID = String.format("%07d", Integer.parseInt(values[0]));
    try {
      this.year = Integer.parseInt(values[1]);
    } catch (Exception e) {
      this.year = 0; //There is one line that has NULL for its year, hacking this to set 0 for now
    }
    this.title = values[2];
  }
}
