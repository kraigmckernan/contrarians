package kraig;

import kraig.data.MovieMetaData;
import kraig.config.Config;

import java.io.*;
import java.util.*;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;

public class MovieMetaDataService {
  private HashMap<String, MovieMetaData> map = new HashMap<String, MovieMetaData>();

  public MovieMetaData getMovieMetaData(String id) {
    return map.get(id);
  }

  public MovieMetaDataService() {
      System.out.println("Reading movie metadata");

      try {
        FileSystem fs = FileSystem.get(Config.conf);
        BufferedReader moviesMetaReader = new BufferedReader(new InputStreamReader(fs.open(new Path(Config.movieMetaDataFile))));
        HashMap<String, MovieMetaData> movieIdToMeta = new HashMap<String, MovieMetaData>();
        String metaLine = moviesMetaReader.readLine();
        while (metaLine != null) {
          MovieMetaData meta = new MovieMetaData(metaLine.trim());
          movieIdToMeta.put(meta.movieID, meta);
          metaLine = moviesMetaReader.readLine();
        }
        this.map = movieIdToMeta;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
  }


}
