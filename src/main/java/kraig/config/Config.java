package kraig.config;

import kraig.data.AverageRating;

import java.util.*;
import java.io.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;

public class Config {
  // Main Parameters:
  public final static int m = 5;
  public final static int u = 25;
  public final static int r = 50;

  // PATHS
  public final static String movieMetaDataFile = "/user/kraig.mckernan/netflix/movie_titles.txt";
  public final static String tempOutput = "/user/kraig.mckernan/tmp/";
  public final static String ratingsFile = tempOutput + "ratings/";
  public final static String averageReviewsPerMovie = tempOutput + "movieAverages/";
  public final static String TopMMovies = tempOutput + "topMovies/";
  public final static String averageTopReviews = tempOutput + "averageTopReviews/";
  public final static String contrariansPath = tempOutput + "contrarians/";
  public final static String contrariansTopMovie = tempOutput + "topContrarianMovies/";

  //HDFS
  public static Configuration conf = new Configuration();

  //Get Top M movies
  public static List<String> gettopMMovies() {
    Path file = new Path(TopMMovies + "part-r-00000");
    try {
      FileSystem fs = FileSystem.get(conf);
      BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(file)));
      String line = reader.readLine();
      ArrayList<String> list = new ArrayList<String>();
      System.out.println("Top M Movies: ");
      while (line != null) {
        AverageRating avg = new AverageRating(line.trim());
        System.out.println(avg.id);
        list.add(avg.id);
        line = reader.readLine();
      }
      return list;
    } catch (Exception e) {
      System.out.println(e);
      return new ArrayList<String>();
    }
  }

  //Get Contrarians
  public static List<String> getContrarians() {
    Path file = new Path(contrariansPath + "part-r-00000");
    try {
      FileSystem fs = FileSystem.get(conf);
      BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(file)));
      String line = reader.readLine();
      ArrayList<String> list = new ArrayList<String>();
      System.out.println("Contrarians: ");
      while (line != null) {
        System.out.println(line.trim());
        list.add(line.trim());
        line = reader.readLine();
      }
      return list;
    } catch (Exception e) {
      System.out.println(e);
      return new ArrayList<String>();
    }
  }
}
