package kraig;

import kraig.MovieMetaDataService;
import kraig.data.Rating;
import kraig.data.MovieMetaData;
import kraig.config.Config;

import java.util.*;
import java.io.*;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;

import java.nio.file.Files;
import java.nio.charset.Charset;

public class MapContrarianMoviesToMovieMeta {

  public static void MapData() {
    try {
      FileSystem fs = FileSystem.get(Config.conf);

      System.out.println("Reading Top Movies for each contrarian");
      BufferedReader contrariansRatingReader = new BufferedReader(new InputStreamReader(fs.open(new Path(Config.contrariansTopMovie + "part-r-00000"))));

      ArrayList<Rating> ratings = new ArrayList<Rating>();
      String line = contrariansRatingReader.readLine();
      while (line != null) {
          Rating rating = new Rating(line.trim());
          ratings.add(rating);
          line = contrariansRatingReader.readLine();
      }

      //----

      System.out.println("Mapping each review and creating csv at csv-output.csv");
      StringBuilder str = new StringBuilder();
      MovieMetaDataService metaService = new MovieMetaDataService();
      for (Rating rating : ratings) {
        MovieMetaData metadata = metaService.getMovieMetaData(rating.movieID);
        str.append(rating.userID + "," + metadata.title + "," + metadata.year + "," + rating.date + '\n');
      }

      List<String> lines = Arrays.asList("UserID,Title,Year,Date", str.toString());
      java.nio.file.Path file = java.nio.file.Paths.get("csv-output.csv");
      Files.write(file, lines, Charset.forName("UTF-8"));

    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
    }
  }

}
