package kraig.runners;

import kraig.MapContrarianMoviesToMovieMeta;
import kraig.mapred.*;
import kraig.config.Config;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class Runner {
  private static Configuration conf = Config.conf;

  public static boolean createRatings(String input, String output) throws Exception {
    Job job = Job.getInstance(conf, "Create Ratings Files");
    job.setJarByClass(CreateRatings.class);
    job.setMapperClass(CreateRatings.AppendMovieIDMapper.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);

    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    FileInputFormat.addInputPath(job, new Path(input));
    SequenceFileOutputFormat.setOutputPath(job, new Path(output));

    return job.waitForCompletion(true);
  }

  public static boolean averageReviews(String ratingsFile, String output) throws Exception {
    Job job = Job.getInstance(conf, "Average Reviews for each movie");
    job.setJarByClass(GroupByRatingField.class);
    job.setMapperClass(GroupByRatingField.GroupByMovieMapper.class);
    job.setReducerClass(AverageRatings.AverageRatingReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);

    job.setInputFormatClass(SequenceFileInputFormat.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    SequenceFileInputFormat.addInputPath(job, new Path(ratingsFile));
    SequenceFileOutputFormat.setOutputPath(job, new Path(output));

    return job.waitForCompletion(true);
  }

  public static boolean gettopMMovies(String averageReviewsPerMovie, String output) throws Exception {
    Job job = Job.getInstance(conf, "Top M Movies");
    job.setJarByClass(TopMMovies.class);
    job.setMapperClass(TopMMovies.TopMSortMapper.class);
    job.setReducerClass(TopMMovies.TopMLimiter.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    job.setNumReduceTasks(1);

    job.setInputFormatClass(SequenceFileInputFormat.class);
    SequenceFileInputFormat.addInputPath(job, new Path(averageReviewsPerMovie));
    FileOutputFormat.setOutputPath(job, new Path(output));

    return job.waitForCompletion(true);
  }

  public static boolean averageReviewsForTopMovies(String topMovieReviews, String output) throws Exception {
    Job job = Job.getInstance(conf, "Average Reviews for each top M movie");
    job.setJarByClass(FilterTopMovieRatings.class);
    job.setMapperClass(FilterTopMovieRatings.FilterTopMovieRatingsMapper.class);
    job.setReducerClass(AverageRatings.AverageRatingReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);

    job.setInputFormatClass(SequenceFileInputFormat.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    SequenceFileInputFormat.addInputPath(job, new Path(topMovieReviews));
    SequenceFileOutputFormat.setOutputPath(job, new Path(output));

    return job.waitForCompletion(true);
  }

  public static boolean getContrarians(String averageTopReviews, String output) throws Exception {
    Job job = Job.getInstance(conf, "Get the contrarians");
    job.setJarByClass(TopUContrarians.class);
    job.setMapperClass(TopUContrarians.FilterAllMReviews.class);
    job.setReducerClass(TopUContrarians.TopULimiter.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    job.setNumReduceTasks(1);

    job.setInputFormatClass(SequenceFileInputFormat.class);
    SequenceFileInputFormat.addInputPath(job, new Path(averageTopReviews));
    FileOutputFormat.setOutputPath(job, new Path(output));

    return job.waitForCompletion(true);
  }

  public static boolean topMoviesForContrarians(String ratingsFile, String output) throws Exception {
    Job job = Job.getInstance(conf, "Get the top movies for each contrarian");
    job.setJarByClass(TopContrarianMovies.class);
    job.setMapperClass(TopContrarianMovies.FilterContrarianReviews.class);
    job.setReducerClass(TopContrarianMovies.TopMovieForUser.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);

    job.setInputFormatClass(SequenceFileInputFormat.class);
    SequenceFileInputFormat.addInputPath(job, new Path(ratingsFile));
    FileOutputFormat.setOutputPath(job, new Path(output));

    return job.waitForCompletion(true);
  }

  public static void runOrExit(boolean passed) {
      if (!passed)
        System.exit(-1);
  }

  public static void println(String str) {
    System.out.println(str);
  }

  public static void main(String[] args) throws Exception {
    println("Input: " + args[0]);
    String input = args[0];

    runOrExit(createRatings(input, Config.ratingsFile));

    runOrExit(averageReviews(Config.ratingsFile, Config.averageReviewsPerMovie));

    runOrExit(gettopMMovies(Config.averageReviewsPerMovie, Config.TopMMovies));

    runOrExit(averageReviewsForTopMovies(Config.ratingsFile, Config.averageTopReviews));

    runOrExit(getContrarians(Config.averageTopReviews, Config.contrariansPath));

    runOrExit(topMoviesForContrarians(Config.ratingsFile, Config.contrariansTopMovie));

    println("Finished all mapreduce");

    MapContrarianMoviesToMovieMeta.MapData();
  }
}
