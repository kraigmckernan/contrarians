package kraig.mapred;

import kraig.data.*;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


// Averages ratings given in the value iterator
public class AverageRatings {
    public static AverageRating createAvgRating(Text id, Iterable<Text> ratings) {
       ArrayList<Rating> listRatings = new ArrayList<Rating>();
       for (Text val : ratings) {
           listRatings.add(new Rating(val.toString()));
       }

      return  new AverageRating(id.toString(), listRatings);
    }

    public static class AverageRatingReducer
        extends Reducer<Text, Text, Text, Text> {
        // Takes a <ID, Rating> and returns an <AverageRating, Nothing>
        private Text nothing = new Text();
        private Text out = new Text();

        public void reduce(Text id, Iterable<Text> ratings, Context context)  throws IOException, InterruptedException {
            AverageRating avg = createAvgRating(id, ratings);

            out.set(avg.output());

            context.write(out, nothing);
        }
    }
}
