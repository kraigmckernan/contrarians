package kraig.mapred;

import kraig.data.*;
import kraig.config.Config;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


// Only outputs movie in the top m list
// Detail: Also outputs userId for the AverageRatingReducer that follows
public class FilterTopMovieRatings {
    public static class FilterTopMovieRatingsMapper
      extends Mapper<Text, Text, Text, Text> {

      List<String> ids = Config.gettopMMovies();

      Text out = new Text();
      //Takes a <Rating, Nothing> and returns a <UserID, Rating> if movieid is in the list
      public void map(Text key, Text nothing, Context context) throws IOException, InterruptedException {
          Rating rating = new Rating(key.toString());

          if (ids.contains(rating.movieID)) {
            out.set(rating.userID);
            context.write(out, key);
          }
      }
    }
}
