package kraig.mapred;

import kraig.data.*;
import kraig.config.Config;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


public class TopMMovies {
    public static class TopMSortMapper
        extends Mapper<Text, Text, Text, Text> {
        private Text kout = new Text();
        private Text vout = new Text();
        // Takes a <AverageRating, Nothing> outputs the sort values <TopMSort, AverageRating> if reviewers is large enough
        public void map(Text key, Text value, Context context
                      ) throws IOException, InterruptedException {
          AverageRating avg = new AverageRating(key.toString());
          if (avg.count >= Config.r) {
            TopMSort sortKey = new TopMSort(avg);

            vout.set(avg.output());
            kout.set(sortKey.output());

            context.write(kout, vout);
          }
      }
    }



    // WARNING: Assumes use of 1 reducer to grab top "M" values
    public static class TopMLimiter
        extends Reducer<Text, Text, Text, Text> {
        // Takes a <Nothing(used for sorting), AverageRating> and returns "M" <AverageRating, Nothing>s
        private Text nothing = new Text();
        private Text out = new Text();
        private int i = 0;

        public void reduce(Text sort, Iterable<Text> ratings, Context context)  throws IOException, InterruptedException {
            Iterator<Text> it = ratings.iterator();
            if (it.hasNext() && i < Config.m) {
              context.write(it.next(), nothing);
              i++;
            }
        }
    }
}
