package kraig.mapred;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


// Input is raw ratings files(IE: mv_xxxxxxx.txt)
// Output is a kraig.data.Rating csv line
//
// Gets the movieId from the filename and appends it to the existing line
public class CreateRatings {
  public static class AppendMovieIDMapper
       extends Mapper<Object, Text, Text, Text>{

    private final static Text nothing = new Text("");

    public int getMovieID(String fileName) {
      //http://stackoverflow.com/questions/4030928/extract-digits-from-a-string-in-java
      return Integer.parseInt(fileName.replaceAll("\\D+", ""));
    }

    public Text appendMovieID(Text orig, String fileName) {
      String allFields = orig.toString() + "," + String.format("%07d", getMovieID(fileName));
      orig.set(allFields);
      return orig;
    }

    //WARNING: this relies on a 1-file to 1 mapper relationship
    private String currentId;

    // takes a <(UserID, rating, date), nothing> and returns a <Rating, Nothing>
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      String str = value.toString();
      if (str.split(",").length == 1) {
        currentId = str.split(":")[0];
        return;
      }

      context.write(appendMovieID(value, currentId), nothing);
    }
  }
}
