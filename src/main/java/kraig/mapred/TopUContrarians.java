package kraig.mapred;

import kraig.data.*;
import kraig.config.Config;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


public class TopUContrarians {
    public static class FilterAllMReviews
      extends Mapper<Text, Text, Text, Text> {
      Text out = new Text();

      //Takes a <AverageRating, Nothing> and returns a <TopUSort, AverageRating> if enough ratings are present
      public void map(Text rating, Text nothing, Context context) throws IOException, InterruptedException {
          AverageRating avg = new AverageRating(rating.toString());

          if (avg.count >= Config.m) {
              out.set(new TopUSort(avg).output());
              context.write(out, rating);
          }
      }
    }


    //WARNING: Assumes use of 1 reducer to only grab top u users
    public static class TopULimiter
        extends Reducer<Text, Text, Text, Text> {
        // Takes a <TopUSort, AverageRating> and return an  <UserID, Nothing>

        private int i = 0;
        private Text out = new Text();
        private Text nothing = new Text();

        public void reduce(Text sort, Iterable<Text> ratings, Context context)  throws IOException, InterruptedException {
          Iterator<Text> it = ratings.iterator();
          if (it.hasNext() && i < Config.u) {
              AverageRating rating = new AverageRating(it.next().toString());

              out.set(rating.id);
              context.write(out, nothing);
              i++;
          }
        }
    }
}
