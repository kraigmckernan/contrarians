package kraig.mapred;

import kraig.data.Rating;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


// outputs key,value so that MR groups specific fields for Ratings
public class GroupByRatingField {
  public static class GroupByMovieMapper
       extends Mapper<Text, Text, Text, Text> {
        // Takes a <Rating, nothing> and returns the field specified by the class given <field, Rating>

    Text id = new Text();

    public String groupByField(Rating rating) {
        return rating.movieID;
    }

    public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
      Rating rating = new Rating(key.toString());

      id.set(groupByField(rating));

      context.write(id, key);
    }
  }

  public static class GroupByUserMapper extends GroupByMovieMapper {
      @Override
      public String groupByField(Rating rating) {
          return rating.userID;
      }
  }
}
