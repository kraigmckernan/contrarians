package kraig.mapred;

import kraig.MovieMetaDataService;
import kraig.data.*;
import kraig.config.Config;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;


public class TopContrarianMovies {
    public static class FilterContrarianReviews
      extends Mapper<Text, Text, Text, Text> {

      List<String> ids = Config.getContrarians();

      Text out = new Text();
      //Takes a <Rating, Nothing> and returns a <UserID, Rating> if movieid is in the list
      public void map(Text key, Text nothing, Context context) throws IOException, InterruptedException {
          Rating rating = new Rating(key.toString());

          if (ids.contains(rating.userID)) {
            out.set(rating.userID);
            context.write(out, key);
          }
      }
    }


    public static class TopMovieForUser
        extends Reducer<Text, Text, Text, Text> {
        // Takes a <UserID, Rating> and return an  <Rating, Nothing>

        private Text out = new Text();
        private Text nothing = new Text();
        private MovieMetaDataService meta = new MovieMetaDataService();

        public void reduce(Text userID, Iterable<Text> ratings, Context context)  throws IOException, InterruptedException {
            Rating max = new Rating(0);

            for (Text r : ratings) {
              Rating rating = new Rating(r.toString());

              // This is bad code
              // I would clean this up if I had more time
              if (rating.rating == max.rating) {
                MovieMetaData maxMovie = meta.getMovieMetaData(max.movieID);
                MovieMetaData thisMovie = meta.getMovieMetaData(rating.movieID);

                if (thisMovie.year == maxMovie.year) {
                  max = (thisMovie.title.compareTo(maxMovie.title) < 0) ? rating : max;
                } else {
                  max = (thisMovie.year > maxMovie.year) ? rating : max;
                }

              }  else if (rating.rating > max.rating) {
                  max = rating;
              }
            }
            out.set(max.output());

            context.write(out, nothing);
        }
    }
}
