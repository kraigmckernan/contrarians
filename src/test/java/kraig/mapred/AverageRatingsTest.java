package kraig.mapred;

import kraig.mapred.*;
import kraig.data.AverageRating;

import java.util.ArrayList;
import org.apache.hadoop.io.Text;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Assert;

public class AverageRatingsTest
    extends TestCase
{
    public AverageRatings testClass = new AverageRatings();

    public void testCases() {
      ArrayList<String> ratings = new ArrayList<String>();
      float expected = 0f;

      ratings.clear();
      ratings.add("1,1,2016,10");
      ratings.add("1,2,2016,10");
      ratings.add("1,3,2016,10");
      ratings.add("1,4,2016,10");
      ratings.add("1,5,2016,10");
      expected = 3f;
      testAverageWorks(ratings, expected);

      ratings.clear();
      ratings.add("1,1,2016,10");
      ratings.add("1,2,2016,10");
      ratings.add("1,3,2016,10");
      ratings.add("1,4,2016,10");
      ratings.add("1,5,2016,10");
      ratings.add("1,5,2016,10");
      expected = 3.33333333f;
      testAverageWorks(ratings, expected);

      ratings.clear();
      ratings.add("1,1,2016,10");
      ratings.add("1,2,2016,10");
      ratings.add("1,3,2016,10");
      ratings.add("1,4,2016,10");
      ratings.add("1,4,2016,10");
      ratings.add("1,5,2016,10");
      expected = 3.16666666f;
      testAverageWorks(ratings, expected);
    }

    float epsilon = .000000001f;

    public void testAverageWorks(ArrayList<String> ratings, float expected) {
      ArrayList<Text> list = new ArrayList<Text>();
      for (String rating : ratings) {
          list.add(new Text(rating));
      }
      AverageRating avg = testClass.createAvgRating(new Text("10"), list);

      Assert.assertEquals(expected, avg.averageRating, epsilon);
    }

}
