package kraig.mapred;

import kraig.mapred.CreateRatings;

import org.apache.hadoop.io.Text;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Assert;

public class CreateRatingsTest
    extends TestCase
{
    CreateRatings.AppendMovieIDMapper mapreduce = new CreateRatings.AppendMovieIDMapper();
    public void test() {
        assertTrue(true);
    }

    public void testCases() {
        testGetMovieIDFromFileName("mv_0000050.txt", 50);
        testGetMovieIDFromFileName("mv_9999999.txt", 9999999);
        testGetMovieIDFromFileName("mv_1234050.txt", 1234050);
        testGetMovieIDFromFileName("mv_0010050.txt", 10050);
        testGetMovieIDFromFileName("mv_0001050.txt", 1050);
        testGetMovieIDFromFileName("mv_6788888.txt", 6788888);
        testGetMovieIDFromFileName("mv_6788888.txt", 6788888);
        testGetMovieIDFromFileName("mv_6788888.txt", 67888234, true);
    }

    public void testGetMovieIDFromFileName(String fileName, int expected) {
        testGetMovieIDFromFileName(fileName, expected, false);
    }

    public void testGetMovieIDFromFileName(String fileName, int expected, boolean shouldFail) {
        int id = mapreduce.getMovieID(fileName);

        if (shouldFail)
          Assert.assertFalse(expected == id);
        else
          Assert.assertEquals(expected, id);
    }
}
