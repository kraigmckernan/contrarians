package kraig.data;

import kraig.data.*;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Assert;

public class AverageRatingTest
    extends TestCase
{
    public void testCases() {
      ArrayList<Rating> ratings = new ArrayList<Rating>();
      float expected = 0f;

      ratings.clear();
      ratings.add(new Rating("1,1,2016,10"));
      ratings.add(new Rating("1,2,2016,10"));
      ratings.add(new Rating("1,3,2016,10"));
      ratings.add(new Rating("1,4,2016,10"));
      ratings.add(new Rating("1,5,2016,10"));
      expected = 3f;
      testAverageWorks(ratings, expected);

      ratings.clear();
      ratings.add(new Rating("1,1,2016,10"));
      ratings.add(new Rating("1,2,2016,10"));
      ratings.add(new Rating("1,3,2016,10"));
      ratings.add(new Rating("1,4,2016,10"));
      ratings.add(new Rating("1,5,2016,10"));
      ratings.add(new Rating("1,5,2016,10"));
      expected = 3.33333333f;
      testAverageWorks(ratings, expected);

      ratings.clear();
      ratings.add(new Rating("1,1,2016,10"));
      ratings.add(new Rating("1,2,2016,10"));
      ratings.add(new Rating("1,3,2016,10"));
      ratings.add(new Rating("1,4,2016,10"));
      ratings.add(new Rating("1,4,2016,10"));
      ratings.add(new Rating("1,5,2016,10"));
      expected = 3.16666666f;
      testAverageWorks(ratings, expected);
    }

    float epsilon = .000000001f;

    public void testAverageWorks(ArrayList<Rating> ratings, float expected) {
      AverageRating avg = new AverageRating("10", ratings);

      Assert.assertEquals(expected, avg.averageRating, epsilon);
    }

}
