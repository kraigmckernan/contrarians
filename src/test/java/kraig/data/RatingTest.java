package kraig.data;

import kraig.data.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Assert;

public class RatingTest
    extends TestCase
{
    public void testBasicRating() {
        String csv = "10,3,2017,12";

        Rating rating = new Rating(csv);

        Assert.assertEquals("10", rating.userID);
        Assert.assertEquals(3, rating.rating);
        Assert.assertEquals("2017", rating.date);
        Assert.assertEquals("12", rating.movieID);
    }
}
