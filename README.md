My notes:
====

This is a pretty barebones answer to the project, there isn't anything super fancy going on. You can see the basic data flow in src/main/java/kraig/runners/Runner.java.


I didn't have a cluster or java project setup or anything. This is basically all from scratch. I tried to complete as fast as I could but I definitely spent at least 3-4 hours just getting things like My one node cluster, maven, unit tests, the way the jobs are pipelined together etc....

I wasn't able to setup meaningful unit tests for the mapreduce phases(IE with mocks so I can test exact input and output) because of time as well. I was only able to test simple things with no dependencies.

I also didn't have time to setup meaningful error handling or proper configuration, so many things are hardcoded(still easily changeable).



I followed the basic approach(modeled in SQL, not tested) for my solution:
----

tables: movies(id, year, title), ratings(movieID, userID, rating, date) where I used the CreateRatings step to create a ratingsFile(basically the ratings table)

```average_ratings_per_movie = SELECT movieID, count(*), avg(rating) FROM ratings GROUP BY movieId```

```top_m_movies = SELECT movieID FROM average_ratings_per_movie WHERE cnt >= R order by avg desc, date desc, title LIMIT M```


```top_m_ratings = SELECT * FROM ratings WHERE movieID IN top_m_movies```

```user_rating_per_movie = SELECT userID, avg(rating), count(*) FROM top_m_ratings GROUP BY userID```

```contrarian_users = SELECT userID FROM user_rating_per_movie WHERE cnt = M order by avg asc, userID asc LIMIT U```


```top_movie_for_contrarians = SELECT max(rating), movieID, userID FROM ratings WHERE user IN contrarian_users GROUP BY userID```


```final_table = SELECT R.userID, M.title, M.year, R.date FROM top_movie_for_contrarians R JOIN movies M on R.movieID = M.movieID```
