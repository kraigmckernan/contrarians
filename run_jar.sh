set -e

TMPOUT="/user/kraig.mckernan/tmp/"
mvn package
hdfs dfs -rm -r -skipTrash "$TMPOUT" || echo "Didn't remove non-existant files on hdfs"
hadoop jar target/contrarian-1.0-SNAPSHOT.jar kraig.runners.Runner /user/kraig.mckernan/netflix/training_set/
